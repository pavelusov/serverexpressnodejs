var express = require('express');
var fs = require('fs');
// var path = require('path');
var app = express();
var users = [];

fs.readFile('users.json', 'utf8', function (err, data) {
    if (err) throw err;
    JSON.parse(data).forEach(function (user) {
        user.name.full = user.name.first + ' ' + user.name.last;
        users.push(user);
    });

});

// pug setting
app.set('views', './views');
app.set('view engine', 'pug');
// pug setting end

// static use
app.use(express.static('public'));

// static use end


app.get('/', function (request, responce) {
    responce.render('index', {users:users});
});

var books = [];
fs.readFile('books.json', 'utf8', function (err, data) {
    if (err) throw err;
    JSON.parse(data).forEach(function (book) {
        books.push(book.book);
    });

});
// for (var i = 0; i < 10000; i++)
// {
//
//     books.push({
//         id:i,
//         title:"Book: " + i,
//         price:i*10
//     }
//     );
// }
app.get('/books', function (req, res) {
    console.time('booksTimer');
    res.render('books', {list:books});
    console.timeEnd('booksTimer');
});
app.get('/book:id', function (req,res) {

    // console.time('bookTimer');
    // var book = books.filter(function (item) {
    //     if (item.id == req.params.id){
    //         return item;
    //     }
    //
    // });
    // 100 000 objects
    // цикл for - по времени на 30% быстрее Array.filter();
    var booksLength = books.length;
    for (var i = 0; i < booksLength;i++)
    {
        if (books[i].id == req.params.id)
        {
            var book = new Object(books[i]);
        }
    }
    // console.timeEnd('bookTimer');
    res.render('book', {book:book});
});
app.get('/:username', function (req, res) {
    var username = req.params.username;
    console.log(req.params);
    res.render('user', {
        data: {
            username: username,
            image: username + '_lg.jpg'
        }
    });
    // console.log(req);
});

var server = app.listen(3000, function () {
    console.log('Server running at http://localhost:' + server.address().port);
});